//dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const validator = require("validator");
const uniqueValidator = require("mongoose-unique-validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

//define your schema
const userSchema = new Schema (
	{
		firstName: {
			type: String,
			trim: true,
			maxlength: 30
		},
		lastName: {
			type: String,
			trim: true,
			maxlength: 30
		},
		username: {
			type: String,
			required: true,
			trim: true,
			maxlength: 30,
			unique: true,
			validate(value) {
				if(!validator.isAlphanumeric(value)){
					throw new Error("Invalid Username. Must contain alphanumeric characters only.")
				}
			}
		},
		email: {
			type: String,
			required: true,
			trim: true,
			unique: true,
			validate(value){
				if(!validator.isEmail(value)){
					throw new Error("Email is invalid!")
				}
			}
		},
		password: {
			type: String,
			required: true,
			minlength: [5, "Password must have at least 5 characters."]
		// },
		// teamId: {
		// 	type: mongoose.Schema.Types.ObjecId,
		// 	ref: "Team"
		},
		isActive: {
			type: Boolean,
			default: true
		},
		profilePic: {
			type: Buffer,
			default: undefined
		},
		role: {
			type: String,
			default: "user",
			enum: ["user", "photographer", "admin"]
		},
		tokens: [
			{
				token: {
					type: String,
					required: true
				}
			}
		]
	},
	{
		timestamps: true,
	}
)

//HASH PASSWORD when creating and updating a user
userSchema.pre("save", async function(next){
	const user = this

	if(user.isModified('password')) {
		//salt
		const salt = await bcrypt.genSalt(10);
		//hash
		user.password = await bcrypt.hash(user.password, salt);
	}

	next();
})

//FIND BY CREDENTIALS USING STATICS
userSchema.statics.findByCredentials = async (email, password) => {
	//check if email exists
	const user = await User.findOne({ email });
	function myError(message) {
		this.message = message
	}

	myError.prototype = new Error()

	const e = {}

	//CHALLENGE
	//DISPLAY ERROR MESSAGES
	//USERNAME or EMAIL
	if(!user) {
		return { "status": "1"}
	}
	//comparing req pw vs unhashed db password
	const isMatch = await bcrypt.compare(password, member.password);

	if(!isMatch) {
		//throw new myError("wrong password")
		return { "status": "2"}
	}

	return user
}

//GENERATE TOKEN
userSchema.methods.generateAuthToken = async function () {
	const user = this

	//data to embed
	//secret message
	//options
	const token = jwt.sign({_id: user._id.toString()}, "approved", { expiresIn: "2 days" })
	//save the token to our db
	user.tokens = user.tokens.concat({ token });
	await user.save();
	return token;
}

//HIDE PRIVATE DATA
userSchema.methods.toJSON = function() {
	const user = this

	//return RAW member object with ALL its fields
	const userObj = user.toObject();

	//delete a private data
	delete userObj.password;

	//delete all tokens
	delete userObj.tokens;

	//delete profilePic
	delete userObj.profilePic;

	return userObj;
}

//SET THE RELATIONSHIP BETWEEN MEMBER AND TASK
userSchema.virtual("tasks", {
	ref: "Task",
	localField: "_id",
	foreignField: "userId"
})

userSchema.plugin(uniqueValidator);
const User = mongoose.model("User", userSchema);
//export your model

module.exports = User;