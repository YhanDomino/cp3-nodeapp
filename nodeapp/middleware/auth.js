const jwt = require("jsonwebtoken");
const User = require("../models/users");

const auth = async (req, res, next) => {
	try {
		//access header
		//get jwt out of the header

		const token = req.header("Authorization").replace("Bearer ", "")

		//make sure that token is valid
		const decoded = jwt.verify(token, "approved")

		//find the member in db
		//check if token is part of member.tokens

		const user = await User.findOne({
			_id: decoded._id,
			"tokens.token": token
		})

		//check if member doesn't exist
		if(!user) {
			throw new Error("User doesn't exist")
		}

		//give routes access to member
		req.user = user

		//give routes access to token
		req.token = token

		//call next function
		next()
	}catch(e){
		//UNAUTHORIZED
		res.status(401).send({ "message": "Please authenticate" });
	}
}

module.exports = auth;