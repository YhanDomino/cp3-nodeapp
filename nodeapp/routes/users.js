//declare your dependencies
const User = require("../models/users");
const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const auth = require("../middleware/auth");
const multer = require("multer");
const sharp = require("sharp");

const upload = multer({
	//dest: "images/members",
	limits: {
		fileSize: 1000000
	},
	fileFilter(req, file, cb){
		if(!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
			return cb (new Error ("Please upload an image file only!"))
		}

		cb(undefined, true)
	}
})

// //UPLOAD AN IMAGE
router.post("/upload", upload.single("upload"), auth, async(req, res) => {

	const buffer = await sharp(req.file.buffer)
		.resize(
	{
		width: 100,
		height: 100
	})
		.png()
		.toBuffer()
	req.user.profilePic = buffer;
	await req.user.save();

	res.send(req.user)
}, (error, req, res, next) => {
	res.status(400).send({error: error.message})

})

//define your routes/endpoints
//CREATE USER
router.post("/", async (req, res) => {
	console.log("create a user")
	const user = new User(req.body);

	try {
		await user.save();
		res.status(201).send(user);
	}catch(e) {
		res.status(400).send(e.message)
	}
});

//GET LOGIN USER'S PROFILE
router.get("/me", auth, async(req, res) => {
	// res.send({req.member, req.token})
	// console.log("test")
	res.send(req.member)
})

// //GET ALL
router.get("/", async (req, res) => {
	try {
		if(!req.query.archived){
			const user = await User.find()
			res.send(user)
		}
		const archive = await User.find({archived: req.query.archived})
		return res.send(archive);
	}catch(e){
		res.status(400).send(e);
	}
})

// //GET ALL
router.get("/", auth, async (req, res) => {
	console.log(req.query)

	try {
		const users = await User.find(req.query).populate({
			path: "teamId",
			select: "name"
		})
		res.status(200).send(users);
			console.log("no return")
	}catch(e){
		res.status(500).send(e.message);
		console.log("after return 2")
	}
});

// //Delete One (deactivating the account)
// // router.delete("/me", auth, async (req, res) =>{
// // 	try {
// // 		await Member.findOneAndDelete({email: req.member.email});
// // 		req.member.archived = true;
// // 		req.member.save()
// // 		req.send(req.member);
// // 	}catch(e) {
// // 		res.status(404).send(e);
// // 	}
// // });

// //LOGIN
router.post("/login", async(req, res) => {
	console.log("fdsafsafsa", req.body)
	try{
		//submit email and password
		//const member = await Member.findByCredentials(req.body.email, req.body.password);
		const user = await User.findOne({email: req.body.email});
		console.log(user, "user2")

		if(!user) {
			console.log("invalid")
			return res.send({"message": "invalid login"})
		}
		console.log("valid")
		//checking if password is correct
		const isMatch = await bcrypt.compare(req.body.password, user.password);

		//res.send(isMatch)
		if(!isMatch) {
			console.log("not match")
			return res.send({"message": "invalid login"})
		}
		console.log("matched")
		//GENERATE a TOKEN
		const token = await user.generateAuthToken();
		res.send({user, token});
	}catch(e) {
		res.status(500).send(e);
	}
})

// //GET ONE /members_id
// router.get("/:id", auth, async(res, send) => {
// 	try {
// 		const member = await Member.findById(req.params.id).populate({
// 			path: "teamId",
// 			select: "name"
// 		})
// 		if(!member) {
// 			res.status(400).send()
// 		}

// 		res.send(member)
// 	}catch(e) {
// 		res.status(500).send(e)
// 	}
// })

//UPLOAD IMAGE
router.get("/:id/upload", async(req, res) => {
	try {
		const user = await User.findById(req.params.id);

		if(!user || !user.profilePic) {
			return res.status(404).send("User or Profile Pic doesn't exist")
		}

		res.set("Content-Type", "image/png")
		res.send(user.profilePic)
	}catch(e) {
		res.status(500).send(e)
	}
})


module.exports = router;