//DECLARE DEPENDENCIES
const express = require("express")
const app = express();
const mongoose = require("mongoose");
const cors = require("cors");
const connectToRemoteDB = require("./config/db");

//CONNECT TO LOCAL DATABASE
// mongoose.connect("mongodb://localhost:27017/mern-capstone3", {
// 	useNewUrlParser: true,
// 	useUnifiedTopology: true,
// 	useFindAndModify: false,
// 	useCreateIndex: true
// });
// mongoose.connection.once("open", ()=> {
// 	console.log("Now connected to local MongoDB");
// });'

//CONNECT TO REMOTEDB
connectToRemoteDB()

//APPLY MIDDLEWARE
app.use(express.json());
app.use(express.urlencoded({  extended:true }));
app.use(cors());

//Declare Models
const User = require("./models/users");

//Declare resources
const usersRoute = require("./routes/users");
app.use("/users", usersRoute);


//initialize the server
app.listen(8000, () => {
	console.log("Now listening to port 8000 CAPSTONE3")
})