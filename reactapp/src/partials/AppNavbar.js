import React, { useState, Fragment} from 'react';
import { Link } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  // UncontrolledDropdown,
  // DropdownToggle,
  // DropdownMenu,
  // DropdownItem,
  Button,
  // NavbarText
} from 'reactstrap';

const AppNavbar = (props) => {
  console.log("AppNavbar props", props.token)
  console.log("AppNavbar props getCurrentUserAttr", props.getCurrentUserAttr)
  console.log("AppNavbar props getCurrentUser", props.getCurrentUser)
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  let authLinks = "";

  if(!props.token) {
    authLinks = (
      <Fragment>
      <Link className="btn btn-primary" to="/login">Login</Link>
      <Link className="btn btn-info ml-3" to="/register">Register</Link>
      </Fragment>
      )
  }else {
    authLinks = (
      <Fragment>
      <Link className="btn btn-primary" to="/profile">Welcome, {props.username}</Link>
      <Link className="btn btn-danger" to="/logout">Logout</Link>
      </Fragment>
      )
  }

  return (
    <div className="mb-5">
      <Navbar color="secondary" light expand="md">
        <NavbarBrand className="font-weight-bold text-white ml-3" href="/" >photoBook</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              {/* <NavLink href="/components/">Users</NavLink> */}
              <Link to="/users" className="nav-link">Users</Link>
            </NavItem>
          </Nav>
          { authLinks }
        </Collapse>
      </Navbar>
    </div>
  );
}

export default AppNavbar;