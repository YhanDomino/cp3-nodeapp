import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';

const RegisterForm = (props) => {
	//State
	console.log(useState("Initial Value"))

	const [formData, setFormData] = useState({
		username: "",
		email: "",
		password: "",
		password2: ""
	})

	const [disabledBtn, setDisabledBtn] = useState(true)

	const [isRedirected, setIsRedirected] = useState(false)

	const { username, email, password, password2 } = formData;

	const onChangeHandler = e => {
		setFormData ({
			...formData,
			[e.target.name] : e.target.value
		})

		console.log(formData);
	}

	const onSubmitHandler = async e => {
		e.preventDefault();

	//check if passwords match
	if(password !== password2 ) {
		console.log("Incorrect Password!")
		Swal.fire({
			title: "Error!",
			text: "Passwords don't Match!",
			icon: "error",
			showConfirmationButton: false,
			timer: 1500
		})
	} else {
		console.log(formData)

		//create a user object
		const newUser = {
			username,
			email,
			password
		}

		try {
			const config = {
				headers: {
					'Content-Type': 'application/json'
				}
			}

			const body = JSON.stringify(newUser)

			//access route using axios
			const res = await axios.post("http://localhost:8000/users", body, config)

			console.log(res)

			//Redirect to login
			setIsRedirected(true)
		} catch(e) {
			console.log(e.response.data.message)
		}
	}
}

//USE EFFECT
//hook that tells the component what to do after render
useEffect(() => {
	if(username !== "" && email !== "" && password !== "" && password2 !== "") {
		setDisabledBtn(false)
	}else {
		setDisabledBtn(true)
	}
}, [formData])

if(isRedirected) {
	return <Redirect to="/login" />
}

   return (
   	<Form onSubmit={ e => onSubmitHandler(e) }>
   			<FormGroup>
   				<Label for="username">Username</Label>
   					<Input type="text" name="username" id="username" value={username} onChange={e => onChangeHandler(e)} maxLength="30" pattern="[a-zA-z0-9]+" required/>
   					<FormText> Use alphanumeric characters only. </FormText>
   			</FormGroup>
			<FormGroup>
   				<Label for="email"> Email </Label>
   					<Input type="email" name="email" id="email" value={email} onChange={e => onChangeHandler(e)} required/>
   			</FormGroup>
   			<FormGroup>
   				<Label for="password">Password</Label>
   					<Input type="password" name="password" id="password" value={password} onChange={e => onChangeHandler(e)} required minLength="5"/>
   			</FormGroup>
   			<FormGroup>
   				<Label for="password2">Confirm Password</Label>
   					<Input type="password" name="password2" id="password2" value={password2} onChange={e => onChangeHandler(e)} required minLength="5"/>
   			</FormGroup>
   				<Button color="primary" className="btn-block mb-3" disabled={disabledBtn}>Submit</Button>
			<p>
				Already have an account? <Link to="/login"> Login </Link>.
			</p>
		</Form>
	);
}

export default RegisterForm;










