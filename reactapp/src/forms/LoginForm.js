import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';

const LoginForm = (props) => {

	const [loginData, setLoginData] = useState ({
		email: "",
		password: ""
	})

	const [disabledBtn, setDisabledBtn] = useState(true)
	const [isRedirected, setIsRedirected] = useState(false)
	const { email, password } = loginData;

	const onChangeHandler = e => {

		setLoginData ({
			...loginData,
			[e.target.name] : e.target.value
		})

	}

	const onSubmitHandler = async e => {
		e.preventDefault();

		const user = {
			email,
			password
		}

		try {
			const config = {
				headers: {
					'Content-Type': 'application/json'
				}
			}

			const body = JSON.stringify(user)

			//access route using axios
			const res = await axios.post("http://localhost:8000/users/login", body, config)
			
			console.log(res)
			localStorage.setItem("token", res.data.token)
			localStorage.setItem("username", res.data.member.username)

			setIsRedirected(true)
		} catch(e) {
			localStorage.removeItem("token")
			console.log(e.response)
		}
	}

	useEffect(() => {
		if(email !== "" && password !== "") {
			setDisabledBtn(false)
		} else {
			setDisabledBtn(true)
		}
	}, [loginData])

	if(isRedirected) {

		//return <Redirect to="/profile" />
		window.location = "profile"
	}

	return (
		<Form onSubmit = {e => onSubmitHandler(e) }>
			<FormGroup>
				<Label for="Email">Email</Label>
					<Input type="email" name="email" placeholder="" value={email} onChange={e => onChangeHandler(e)} maxLength="30"/>
			</FormGroup>
			<FormGroup>
				<Label for="password">Password</Label>
					<Input type="password" name="password" placeholder="" value={password} onChange={e => onChangeHandler(e)} required minLenght="5" />
			</FormGroup>
		<Button color="primary" block disabled={disabledBtn}> Login </Button>
		<p>
			Don't have an account yet? <Link to="/register"> Register </Link>
		</p>
		</Form>
	)
}

export default LoginForm;