import React from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
import App from './App';

const root = document.querySelector("#root")

ReactDOM.render(<App />, document.getElementById('root'));

const pageComponent = <App/>

ReactDOM.render(pageComponent, root);