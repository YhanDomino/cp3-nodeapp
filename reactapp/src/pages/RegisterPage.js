import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import RegisterForm from '../forms/RegisterForm';

const RegisterPage = (props) => {
	return (
		<Container>
			<Row>
				<Col>
					<h1> Register Here: </h1>
				</Col>
			</Row>
			<Row>
				<Col>
					<RegisterForm/>
				</Col>
			</Row>
		</Container>
	)
}

export default RegisterPage;