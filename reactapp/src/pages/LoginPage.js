import React from 'react';
import { Jumbotron, Container, Col, Row } from 'reactstrap';
import { Redirect } from 'react-router-dom';
import LoginForm from '../forms/LoginForm';

const LoginPage = (props) => {
	console.log("LoginPage props", props.token)

	return (
		<Jumbotron>
			<Row className="mb-3">
			<Col>
				<h1> Login Page </h1>
			</Col>
			</Row>
			<Row>
				<Col md="4" className="border">
					<LoginForm/>
				</Col>
			</Row>
		</Jumbotron>
	)
}

export default LoginPage;