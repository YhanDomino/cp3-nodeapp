import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'reactstrap';
import UserProfileForm from '../forms/UserProfileForm';
import axios from 'axios'

const UserProfilePage = (props) => {
	const [ userData, setUserData ] = useState({
		token: props.token,
		member: {}
	})

	const { token, user } = userData

	//GET USER
	const getUser = async () => {
		try {
			const config = {
				headers: {
					Authorization: `Bearer ${token}`
				}
			}

			const res = await axios.get(`http://localhost:8000/users/${props.match.params.id}`, config)

			setUserData({
				...userData,
				member: res.data
			})
		} catch(e) {
			console.log(e)
		}
	} 

	useEffect(() => {
		getUser()
	}, [setUserData])

	//POPULATE DROPDOWN

	return (
		<Container className="my-5">
			<Row className="mb-3">
				<Col>
					<h1> User Profile Page </h1>
				</Col>
			</Row>
			<Row>
				<Col>
					<UserProfileForm user={user} />
				</Col>
			</Row>
		</Container>
	)
}

export default UserProfilePage;