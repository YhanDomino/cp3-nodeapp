import React from 'react';
import { Container, Row, Col } from 'reactstrap';

const ErrorPage = (props) => {
	return (
  		<Container className="my-5">
    		<h3>PAGE NOT FOUND </h3>
    		<p>We are sorry but the page you are looking for does not exist.</p>
  		</Container>
	)
}

export default ErrorPage;