import React, { useState }from 'react';
import { Redirect, BrowserRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css";

//pages
import AppNavbar from './partials/AppNavbar';
// import MembersPage from './pages/MembersPage';
// import MemberTable from './tables/MemberTable';
// import TeamsPage from './pages/TeamsPage';
// import TeamsProfilePage from './pages/TeamsProfilePage';
// import TaskPage from './pages/TaskPage';
import ErrorPage from './pages/ErrorPage';
import LoginPage from './pages/LoginPage';
import ProfilePage from './pages/ProfilePage';
import HomePage from './pages/HomePage';
import RegisterPage from './pages/RegisterPage';
// import MemberProfilePage from './pages/MemberProfilePage';
// import TaskProfilePage from './pages/TaskProfilePage';


const App = () => {

  const [appData, setAppData] = useState({
    token: localStorage.token,
    username: localStorage.username
  })

  const { token, username } = appData;

  const getCurrentUser = () => {
    return { username, token }
  }

  // //UsersPage
  const Load = (props, page) => {
    console.log("App.js ! token", token !== undefined)
    console.log("App.js token", token === undefined)

  // // //Home Page
  if(page === "HomePage") return <HomePage {...props} />

  // // //login page
  if(page === "LoginPage" && token ) {
    return <Redirect to="/profile" />
  }else if(page === "LoginPage") {
    return <LoginPage {...props} />
  }

  // // //register page
  if(page === "RegisterPage" && token ) {
    return <Redirect to="/login" />
  } else if(page === "RegisterPage") {
    return <RegisterPage {...props}/>
  }

  // //if token === null return <Redirect to="login"/>
    if(!token) return <Redirect to="/login" />

      if( page === "LogoutPage") {
        localStorage.clear()
        setAppData({
          token,
          username
        })
        return window.location = "/login"
      }

    switch(page) {
      case "HomePage" : return <LoginPage {...props} token={token} />

      default: return <ErrorPage/>
    }
  
  }

    return (
      <BrowserRouter>
        <AppNavbar token={ token } username={ username } getCurrentUser={ getCurrentUser()} />
          <Switch>
           <Route path="/login" render={ (props) => Load(props, "LoginPage")} />
           <Route path="/profile" render= { (props)=> Load(props, "ProfilePage")} />
           <Route path="/register" render= { (props) => Load(props, "RegisterPage")} />
           <Route exact path="/" render= { (props) => Load(props, "HomePage")} />
        <Route patch="*" render={ (props)=> Load(props, "ErrorPage")} />
         </Switch>
      </BrowserRouter>
    )
}
export default App;